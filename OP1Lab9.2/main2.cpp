#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <Windows.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int exit;
	do {
		exit = -1;
		printf("\n������������ ������� � ���� ����� � �������� ����� �������� �������������� �����. ������ ������������ �����.\n->");

		int num, sum, mult;

		scanf("%d", &num);
		if (num < 1000 || num > 9999) {
			printf("\n����� �� � �������������\n");
			continue;
		}
		sum = num % 10 + num / 100 % 10;
		mult = num % 10 * (num / 100 % 10);
		printf("���� ����� � �������� ����� = %d, ������� = %d\n", sum, mult);

		while (exit != 1 && exit != 2) {
			printf("\n��� ����������� ������ ������ 1, ��� ���������� 2: ");
			scanf("%d", &exit);
		};

	} while (exit != 2);
	return 0;
}