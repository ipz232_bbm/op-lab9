#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <windows.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int exit;

	do {
		exit = -1;
		double seconds;
		do {
			printf("������ ����� ������, ���� ������� ���� �������� ������\n");
			printf("->");
			scanf("%lf", &seconds);
		} while (seconds < 0);
		Sleep(seconds * 1000);
		printf("\a*����*");

		while (exit != 1 && exit != 2) {
			printf("\n��� ����������� ������ ������ 1, ��� ���������� 2: ");
			scanf("%d", &exit);
		};

	} while (exit != 2);
	return 0;
}