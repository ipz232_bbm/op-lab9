#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <Windows.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int exit;

	do {
		exit = -1;
		printf("\n���������� ���������� ������������� ����������� ����� �� ����������� �����. ��� ���������� �������� ������ ����.\n");

		int n = 0;
		double max, avg = 0, num;

		printf("->");
		scanf("%lf", &num);
		max = num;

		while (num != 0) {
			if (num > max) max = num;
			avg += num;
			n++;
			printf("->");
			scanf("%lf", &num);
		}

		avg /= n;
		printf("������� �����: %d\n", n);
		printf("����������� ������� �������� = %.2f, ������� ����������� = %.2f", max, avg);

		while (exit != 1 && exit != 2) {
			printf("\n��� ����������� ������ ������ 1, ��� ���������� 2: ");
			scanf("%d", &exit);
		};

	} while (exit != 2);
	return 0;
}